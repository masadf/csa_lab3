from enum import Enum
from typing import NamedTuple

INPUT = 5555
OUTPUT = 5556
OUTPUT_NUM = 5557


class OperationType(Enum):
    ARG_DATA = 1
    ARG_JMP = 2
    SINGLE = 3


class AddressType(Enum):
    STRAIGHT = 1
    INDIRECT_DATA = 2
    RELATIVE_INDIRECT = 3
    WITHOUT = 4


class OperationFormat(NamedTuple):
    number: int
    type: OperationType
    mnemonic_format: str


class Operation(OperationFormat, Enum):
    LD = OperationFormat(1, OperationType.ARG_DATA, "LD %s: acc <- %s")  # Загрузить значение в аккумулятор
    SV = OperationFormat(2, OperationType.ARG_DATA, "SV %s: acc -> %s")  # Сохранить в память по адресу
    ADD = OperationFormat(3, OperationType.ARG_DATA, "ADD %s: acc <- acc + %s")
    SUB = OperationFormat(4, OperationType.ARG_DATA, "SUB %s: acc <- acc - %s")
    CMP = OperationFormat(5, OperationType.ARG_DATA, "CMP %s: flags(acc - %s)")
    JMP = OperationFormat(6, OperationType.ARG_JMP, "JMP %s: ip <- %s")
    JE = OperationFormat(7, OperationType.ARG_JMP, "JE %s: Z=1 ? ip <- %s")  # Переход, если Z=1
    JNE = OperationFormat(8, OperationType.ARG_JMP, "JNE %s: Z=0 ? ip <- %s")  # Переход, если Z=0
    CALL = OperationFormat(
        9, OperationType.ARG_JMP, "CALL %s: sr <- sr - 1; mem(sr) <- ip; ip <- %s"
    )  # Переход в функцию
    RET = OperationFormat(10, OperationType.SINGLE, "RET: ip <- mem(sr); sr <- sr + 1")  # Возврат из функции
    MUL = OperationFormat(11, OperationType.ARG_DATA, "MUL %s: acc <- acc * %s")  # Умножить
    DIV = OperationFormat(12, OperationType.ARG_DATA, "DIV %s: acc <- acc / %s")  # Разделить
    INC = OperationFormat(
        13, OperationType.ARG_DATA, "INC %s: %s <- %s + 1"
    )  # Инкремент значения по адресу без замены acc
    DEC = OperationFormat(
        14, OperationType.ARG_DATA, "DEC %s: %s <- %s - 1"
    )  # Декремент значения по адресу без замены acc
    IRET = OperationFormat(
        15,
        OperationType.SINGLE,
        "IRET: sp <- mem(sr); sr <- sr + 1; ip <- mem(sr); sr <- sr + 1; acc <- mem(sr); sr <- sr + 1",
    )
    HLT = OperationFormat(99, OperationType.SINGLE, "HLT")


opcodes_by_name = dict((opcode.name, opcode) for opcode in Operation)
opcodes_by_value = dict((opcode.number, opcode) for opcode in Operation)
address_type_by_value = dict((addrType.value, addrType) for addrType in AddressType)
