from __future__ import annotations

import argparse
import json
import logging
from enum import Enum

from AluError import AluError
from HLTError import HLTError
from utils import INPUT, OUTPUT, OUTPUT_NUM, AddressType, Operation, address_type_by_value, opcodes_by_value

BIT_DEPTH = 64

mask_64 = 0xFFFFFFFFFFFFFFFF


def bytes_to_int(b) -> int:
    val = 0
    for byte in b:
        val = (val << 8) + byte
    return val


def read_data(f):
    data_length = bytes_to_int(f.read(2))
    assert data_length % 8 == 0, f"unexpected data length, got {data_length}"
    data = []
    while data_length > 0:
        data.append(bytes_to_int(f.read(8)))
        data_length -= 8
    return data


def read_code(src):
    data_offset = bytes_to_int(src.read(2))
    instr_offset = bytes_to_int(src.read(2))
    settings_offset = bytes_to_int(src.read(2))
    magic = bytes_to_int(src.read(2))
    assert magic == 0xC0DE, f"Wrong magic, got {magic}"
    src.seek(data_offset)
    data = read_data(src)
    src.seek(instr_offset)
    instr = read_data(src)
    src.seek(settings_offset)
    settings = read_data(src)[0]

    return data, instr, settings


def main(src, inter):
    data, instr, settings = read_code(src)
    data_path = DataPath(1024, 1024, data, settings)
    interruptions = json.loads(inter.read())
    for i in interruptions:
        if (i[1]) == "\\0":
            i[1] = ord("\0")
        else:
            i[1] = ord(i[1])
    control_unit = ControlUnit(instr, 1024, data_path, interruptions, settings)
    control_unit.start()
    result = "".join([str(token) for token in control_unit.output_tokens])
    with open("result.txt", "w") as file:
        file.write(result)


class AluOp(Enum):
    SUM = "sum"
    MUL = "mul"
    DIV = "div"


class ALU:
    def __init__(self):
        self.operation = AluOp.SUM

        self.left_val = 0
        self.right_val = 0
        self.inverse_left = False
        self.inverse_right = False
        self.incr_left = False
        self.incr_right = False
        self.carry = False
        self.overflow = False
        self.zero = False
        self.negative = False
        self.withFlags = True

    def alu_sum(self) -> int:
        result = 0
        left = ~self.left_val if self.inverse_left else self.left_val
        left = (left + 1) & mask_64 if self.incr_left else left

        right = ~self.right_val if self.inverse_right else self.right_val
        right = (right + 1) & mask_64 if self.incr_right else right

        carry = int(self.carry)

        bites_carry = carry

        for i in range(64):
            bit_res = ((left >> i) & 0x1) + ((right >> i) & 0x1) + ((bites_carry >> i) & 0x1)
            result = result | ((bit_res & 0x1) << i)
            if i == 63 and self.withFlags:
                self.carry = bool((bit_res & 0x2) >> 1)
            else:
                bites_carry = bites_carry | ((bit_res & 0x2) << i)
        if self.withFlags:
            self.overflow = ((int(self.carry) >> 63) & 0x1) != self.carry
            self.zero = result == 0
            self.negative = bool((result >> 63) & 0x1)

        return result & mask_64

    def alu_mul(self) -> int:
        return (self.right_val * self.left_val) & mask_64

    def alu_div(self) -> int:
        return (self.left_val // self.right_val) & mask_64

    def alu_process(self) -> int:
        result = 0

        if self.operation == AluOp.SUM:
            result = self.alu_sum()
        elif self.operation == AluOp.MUL:
            result = self.alu_mul()
        elif self.operation == AluOp.DIV:
            result = self.alu_div()
        else:
            raise AluError

        return result

    def alu_state(self) -> int:
        return (int(self.negative) << 3) + (int(self.zero) << 2) + (int(self.overflow) << 1) + self.carry

    def set_alu_state(self, state):
        self.negative = bool(state >> 3)
        self.zero = bool(state >> 2)
        self.overflow = bool(state >> 1)
        self.negative = bool(state & 0x1)

    def print(self):
        return f"alu_state={int(self.negative)}{int(self.zero)}{int(self.overflow)}{int(self.carry)}"


class DataPath:
    def __init__(self, data_memory_size: int, program_size: int, data_memory: list[int], settings):
        self.data_memory_size = data_memory_size
        self.data_memory = data_memory.copy()
        self.data_memory.extend([0] * (data_memory_size - len(data_memory)))
        self.alu = ALU()

        # registers
        self.acc = 0  # Аккумулятор
        self.ip = (settings >> 32) & 0xFFFFFFFF  # Адрес следующей инструкции
        self.br = 0  # Буферный регистр
        self.ddr = 0  # Регистр данных для записи в память данных
        self.idr = 0  # Регистр данных для записи в память инструкций
        self.iar = 0  # Регистр адреса инструкции
        self.dar = 0  # Регистр адреса данных
        self.ir = 0  # Регистр инструкции
        self.sr = program_size - 1  # Регистр стека

    def print(self):
        return f"{self.alu.print()}; acc={hex(self.acc)}; ip={hex(self.ip)}; br={hex(self.br)}; ddr={hex(self.ddr)}; idr={hex(self.idr)}; iar={hex(self.iar)}; dar={hex(self.dar)}; id={hex(self.ir)}; sr={hex(self.sr)}"


class ControlUnit:
    def __init__(self, program, program_size, data_path: DataPath, input_irq, settings):
        self.program = program.copy()
        self.data_path = data_path
        self.inputIrq: list = input_irq
        self.is_interrupted: bool = False
        self.intAddress = settings & 0xFFFFFFFF
        self.tickNum = 0
        self.program.extend([0] * (program_size - len(program)))
        self.output_tokens = []

        self.executors = {
            Operation.LD: self.ld,
            Operation.SV: self.sv,
            Operation.ADD: self.add,
            Operation.SUB: self.sub,
            Operation.CMP: self.cmp,
            Operation.JMP: self.jmp,
            Operation.JE: self.je,
            Operation.JNE: self.jne,
            Operation.CALL: self.call,
            Operation.RET: self.ret,
            Operation.MUL: self.mul,
            Operation.DIV: self.div,
            Operation.INC: self.inc,
            Operation.DEC: self.dec,
            Operation.IRET: self.iret,
            Operation.HLT: self.hlt,
        }

    def tick(self):
        self.tickNum += 1

    def instruction_fetch(self):
        # Записываем адрес инструкции в регистр, взаимодействующий с памятью
        instruction_address = self.data_path.ip
        self.data_path.iar = instruction_address
        self.tick()

        # Достаём команду и повышаем счётчик команд
        self.data_path.idr = self.program[self.data_path.iar]
        self.data_path.alu.right_val = self.data_path.ip
        self.data_path.alu.left_val = 0
        self.data_path.alu.incr_right = True
        self.data_path.alu.inverse_left = False
        self.data_path.alu.inverse_right = False
        self.data_path.alu.incr_left = False
        self.data_path.alu.carry = False
        self.data_path.alu.withFlags = False
        self.data_path.alu.operation = AluOp.SUM
        self.data_path.ip = self.data_path.alu.alu_process()
        self.tick()

        # Загружаем команду в регистр текущей команды
        self.data_path.ir = self.data_path.idr
        self.tick()

    def data_address_fetch(self):
        instr = self.data_path.ir
        address_type = address_type_by_value[(instr >> 48) & 0xFF]

        if address_type == AddressType.STRAIGHT:
            # В случае с прямой адресацией направляем данные из регистра инструкции в регистр данных через алу
            self.data_path.alu.right_val = self.data_path.ir & ((2**48) - 1)
            self.data_path.alu.left_val = 0
            self.data_path.alu.incr_right = False
            self.data_path.alu.inverse_left = False
            self.data_path.alu.inverse_right = False
            self.data_path.alu.incr_left = False
            self.data_path.alu.carry = False
            self.data_path.alu.withFlags = False
            self.data_path.alu.operation = AluOp.SUM
            self.data_path.ddr = self.data_path.alu.alu_process()
            self.tick()
        elif address_type == AddressType.INDIRECT_DATA:
            # В случае с косвенной адресацией направляем данне из регистра инструкции в регистр выборки операнда через алу
            self.data_path.alu.right_val = self.data_path.ir & (2**48 - 1)
            self.data_path.alu.left_val = 0
            self.data_path.alu.incr_right = False
            self.data_path.alu.inverse_left = False
            self.data_path.alu.inverse_right = False
            self.data_path.alu.incr_left = False
            self.data_path.alu.withFlags = False
            self.data_path.alu.carry = False
            self.data_path.alu.operation = AluOp.SUM
            self.data_path.dar = self.data_path.alu.alu_process()
            self.tick()

            # Далее достаём данные из памяти
            self.data_path.ddr = self.data_path.data_memory[self.data_path.dar]
            self.tick()
        elif address_type == AddressType.RELATIVE_INDIRECT:
            # В случае с косвенно-относительной адресацией направляем данне из регистра инструкции в регистр выборки операнда через алу
            self.data_path.alu.right_val = self.data_path.ir & (2**48 - 1)
            self.data_path.alu.left_val = 0
            self.data_path.alu.incr_right = False
            self.data_path.alu.inverse_left = False
            self.data_path.alu.inverse_right = False
            self.data_path.alu.incr_left = False
            self.data_path.alu.withFlags = False
            self.data_path.alu.carry = False
            self.data_path.alu.operation = AluOp.SUM
            self.data_path.dar = self.data_path.alu.alu_process()
            self.tick()

            # Далее достаём данные из памяти
            self.data_path.ddr = self.data_path.data_memory[self.data_path.dar]
            self.tick()

            # Далее загружаем в idr
            self.data_path.alu.right_val = self.data_path.ddr
            self.data_path.alu.left_val = 0
            self.data_path.alu.incr_right = False
            self.data_path.alu.inverse_left = False
            self.data_path.alu.inverse_right = False
            self.data_path.alu.incr_left = False
            self.data_path.alu.withFlags = False
            self.data_path.alu.carry = False
            self.data_path.alu.operation = AluOp.SUM
            self.data_path.dar = self.data_path.alu.alu_process()
            self.tick()

            # Далее достаём данные из памяти
            self.data_path.ddr = self.data_path.data_memory[self.data_path.dar]
            self.tick()
        else:
            return

    def ld(self):
        # Загружаем в аккумулятор
        if self.data_path.ddr == INPUT:
            self.data_path.acc = self.inputIrq.pop(0)[1]
        else:
            self.data_path.acc = self.data_path.ddr
        self.tick()

    def sv(self):
        # Загружаем в память/output
        if self.data_path.ddr == OUTPUT:
            self.output_tokens.append(chr(self.data_path.acc))
        elif self.data_path.ddr == OUTPUT_NUM:
            self.output_tokens.append(self.data_path.acc)
        else:
            self.data_path.data_memory[self.data_path.dar] = self.data_path.acc
        self.tick()

    def add(self):
        # Повышаем значение аккумулятор и переводим в буферный регистр
        self.data_path.alu.right_val = self.data_path.ddr
        self.data_path.alu.left_val = self.data_path.acc
        self.data_path.alu.incr_right = False
        self.data_path.alu.inverse_left = False
        self.data_path.alu.inverse_right = False
        self.data_path.alu.incr_left = False
        self.data_path.alu.carry = False
        self.data_path.alu.withFlags = True
        self.data_path.alu.operation = AluOp.SUM
        self.data_path.br = self.data_path.alu.alu_process()
        self.tick()

        # Кладём значение в аккумулятор
        self.data_path.acc = self.data_path.br
        self.tick()

    def sub(self):
        # Понижаем значение аккумулятор и переводим в буферный регистр
        self.data_path.alu.right_val = self.data_path.ddr
        self.data_path.alu.left_val = self.data_path.acc
        self.data_path.alu.incr_right = True
        self.data_path.alu.inverse_left = False
        self.data_path.alu.inverse_right = True
        self.data_path.alu.incr_left = False
        self.data_path.alu.carry = False
        self.data_path.alu.withFlags = True
        self.data_path.alu.operation = AluOp.SUM
        self.data_path.br = self.data_path.alu.alu_process()
        self.tick()

        # Кладём значение в аккумулятор
        self.data_path.acc = self.data_path.br
        self.tick()

    def mul(self):
        # Умножаем значение аккумулятор и переводим в буферный регистр
        self.data_path.alu.right_val = self.data_path.ddr
        self.data_path.alu.left_val = self.data_path.acc
        self.data_path.alu.incr_right = False
        self.data_path.alu.inverse_left = False
        self.data_path.alu.inverse_right = False
        self.data_path.alu.incr_left = False
        self.data_path.alu.carry = False
        self.data_path.alu.withFlags = True
        self.data_path.alu.operation = AluOp.MUL
        self.data_path.br = self.data_path.alu.alu_process()
        self.tick()

        # Кладём значение в аккумулятор
        self.data_path.acc = self.data_path.br
        self.tick()

    def div(self):
        # Делим значение аккумулятор и переводим в буферный регистр
        self.data_path.alu.right_val = self.data_path.ddr
        self.data_path.alu.left_val = self.data_path.acc
        self.data_path.alu.incr_right = False
        self.data_path.alu.inverse_left = False
        self.data_path.alu.inverse_right = False
        self.data_path.alu.incr_left = False
        self.data_path.alu.carry = False
        self.data_path.alu.withFlags = True
        self.data_path.alu.operation = AluOp.DIV
        self.data_path.br = self.data_path.alu.alu_process()
        self.tick()

        # Кладём значение в аккумулятор
        self.data_path.acc = self.data_path.br
        self.tick()

    def cmp(self):
        # Аналог SUB, но без записи в регистр
        self.data_path.alu.right_val = self.data_path.ddr
        self.data_path.alu.left_val = self.data_path.acc
        self.data_path.alu.incr_right = True
        self.data_path.alu.inverse_left = False
        self.data_path.alu.inverse_right = True
        self.data_path.alu.incr_left = False
        self.data_path.alu.carry = False
        self.data_path.alu.withFlags = True
        self.data_path.alu.operation = AluOp.SUM
        self.data_path.alu.alu_process()
        self.tick()

    def call(self):
        # Загружаем регистр ip в буфер
        self.data_path.br = self.data_path.ip
        self.tick()

        # Загружаем адрес команды через алу
        self.data_path.alu.right_val = self.data_path.ir & ((2**48) - 1)
        self.data_path.alu.left_val = 0
        self.data_path.alu.incr_right = False
        self.data_path.alu.inverse_left = False
        self.data_path.alu.inverse_right = False
        self.data_path.alu.incr_left = False
        self.data_path.alu.carry = False
        self.data_path.alu.withFlags = True
        self.data_path.alu.operation = AluOp.SUM
        self.data_path.ip = self.data_path.alu.alu_process()

        self.tick()

        # Загружаем значение буферного регистра в idr, а указатель стека в iar
        self.data_path.iar = self.data_path.sr
        self.data_path.idr = self.data_path.br

        self.tick()

        # Сохраняем значение в памяти инструкций и уменьшаем значение sr
        self.program[self.data_path.iar] = self.data_path.idr
        self.data_path.alu.right_val = self.data_path.sr
        self.data_path.alu.left_val = 0
        self.data_path.alu.incr_right = False
        self.data_path.alu.inverse_left = True
        self.data_path.alu.inverse_right = False
        self.data_path.alu.incr_left = False
        self.data_path.alu.carry = False
        self.data_path.alu.withFlags = False
        self.data_path.alu.operation = AluOp.SUM
        self.data_path.br = self.data_path.alu.alu_process()
        self.tick()

        self.data_path.sr = self.data_path.br
        self.tick()

    def ret(self):
        # Загружаем адрес команды через алу с инкрементом
        self.data_path.alu.right_val = self.data_path.sr
        self.data_path.alu.left_val = 0
        self.data_path.alu.incr_right = True
        self.data_path.alu.inverse_left = False
        self.data_path.alu.inverse_right = False
        self.data_path.alu.incr_left = False
        self.data_path.alu.carry = False
        self.data_path.alu.withFlags = False
        self.data_path.alu.operation = AluOp.SUM
        result = self.data_path.alu.alu_process()
        self.data_path.iar = result
        self.data_path.br = result

        self.tick()

        # Загружаем новое значение стека и достаём из памяти cтарое значение ip
        self.data_path.sr = self.data_path.br
        self.data_path.idr = self.program[self.data_path.iar]

        self.tick()

        # Сохраняем значение в регистре ip
        self.data_path.ip = self.data_path.idr
        self.tick()

    def iret(self):
        # Загружаем адрес команды через алу с инкрементом
        self.data_path.alu.right_val = self.data_path.sr
        self.data_path.alu.left_val = 0
        self.data_path.alu.incr_right = True
        self.data_path.alu.inverse_left = False
        self.data_path.alu.inverse_right = False
        self.data_path.alu.incr_left = False
        self.data_path.alu.carry = False
        self.data_path.alu.withFlags = False
        self.data_path.alu.operation = AluOp.SUM
        result = self.data_path.alu.alu_process()
        self.data_path.iar = result
        self.data_path.br = result

        self.tick()

        # Загружаем состоние алу и устанавливаем его
        self.data_path.sr = self.data_path.br
        self.data_path.idr = self.program[self.data_path.iar]

        self.tick()

        self.data_path.alu.set_alu_state(self.data_path.idr)
        self.tick()

        # Загружаем адрес команды через алу с инкрементом
        self.data_path.alu.right_val = self.data_path.sr
        self.data_path.alu.left_val = 0
        self.data_path.alu.incr_right = True
        self.data_path.alu.inverse_left = False
        self.data_path.alu.inverse_right = False
        self.data_path.alu.incr_left = False
        self.data_path.alu.carry = False
        self.data_path.alu.withFlags = False
        self.data_path.alu.operation = AluOp.SUM
        result = self.data_path.alu.alu_process()
        self.data_path.iar = result
        self.data_path.br = result

        self.tick()

        # Загружаем новое значение стека и достаём из памяти cтарое значение ip
        self.data_path.sr = self.data_path.br
        self.data_path.idr = self.program[self.data_path.iar]

        self.tick()

        # Сохраняем значение в регистре ip
        self.data_path.ip = self.data_path.idr
        self.tick()

        # Загружаем адрес команды через алу с инкрементом
        self.data_path.alu.right_val = self.data_path.sr
        self.data_path.alu.left_val = 0
        self.data_path.alu.incr_right = True
        self.data_path.alu.inverse_left = False
        self.data_path.alu.inverse_right = False
        self.data_path.alu.incr_left = False
        self.data_path.alu.carry = False
        self.data_path.alu.withFlags = False
        self.data_path.alu.operation = AluOp.SUM
        result = self.data_path.alu.alu_process()
        self.data_path.iar = result
        self.data_path.br = result

        self.tick()

        # Загружаем новое значение стека и достаём из памяти старое значение аккумулятора
        self.data_path.sr = self.data_path.br
        self.data_path.idr = self.program[self.data_path.iar]

        self.tick()

        # Сохраняем значение в регистре acc
        self.data_path.alu.right_val = self.data_path.idr
        self.data_path.alu.left_val = 0
        self.data_path.alu.incr_right = False
        self.data_path.alu.inverse_left = False
        self.data_path.alu.inverse_right = False
        self.data_path.alu.incr_left = False
        self.data_path.alu.carry = False
        self.data_path.alu.withFlags = False
        self.data_path.alu.operation = AluOp.SUM
        self.data_path.acc = self.data_path.alu.alu_process()

        self.is_interrupted = False

    def jmp(self):
        # Загружаем адрес команды через алу с инкрементом
        self.data_path.ir = self.data_path.idr
        self.data_path.alu.right_val = self.data_path.ir & ((2**48) - 1)
        self.data_path.alu.left_val = 0
        self.data_path.alu.incr_right = False
        self.data_path.alu.inverse_left = False
        self.data_path.alu.inverse_right = False
        self.data_path.alu.incr_left = False
        self.data_path.alu.withFlags = False
        self.data_path.alu.carry = False
        self.data_path.alu.operation = AluOp.SUM
        self.data_path.ip = self.data_path.alu.alu_process()

        self.tick()

    def je(self):
        if self.data_path.alu.zero:
            # Загружаем адрес команды через алу с инкрементом
            self.data_path.ir = self.data_path.idr
            self.data_path.alu.right_val = self.data_path.ir & ((2**48) - 1)
            self.data_path.alu.left_val = 0
            self.data_path.alu.incr_right = False
            self.data_path.alu.inverse_left = False
            self.data_path.alu.inverse_right = False
            self.data_path.alu.incr_left = False
            self.data_path.alu.withFlags = False
            self.data_path.alu.carry = False
            self.data_path.alu.operation = AluOp.SUM
            self.data_path.ip = self.data_path.alu.alu_process()

            self.tick()

    def jne(self):
        if not self.data_path.alu.zero:
            # Загружаем адрес команды через алу с инкрементом
            self.data_path.ir = self.data_path.idr
            self.data_path.alu.right_val = self.data_path.ir & ((2**48) - 1)
            self.data_path.alu.left_val = 0
            self.data_path.alu.incr_right = False
            self.data_path.alu.inverse_left = False
            self.data_path.alu.inverse_right = False
            self.data_path.alu.incr_left = False
            self.data_path.alu.withFlags = False
            self.data_path.alu.carry = False
            self.data_path.alu.operation = AluOp.SUM
            self.data_path.ip = self.data_path.alu.alu_process()

            self.tick()

    def inc(self):
        # Повышаем значение аккумулятор и переводим в буферный регистр
        self.data_path.alu.right_val = self.data_path.ddr
        self.data_path.alu.left_val = 0
        self.data_path.alu.incr_right = True
        self.data_path.alu.inverse_left = False
        self.data_path.alu.inverse_right = False
        self.data_path.alu.incr_left = False
        self.data_path.alu.carry = False
        self.data_path.alu.withFlags = True
        self.data_path.alu.operation = AluOp.SUM
        self.data_path.br = self.data_path.alu.alu_process()
        self.tick()

        # Кладём значение в ddr
        self.data_path.ddr = self.data_path.br
        self.tick()

        # Записываем значение в память данных
        self.data_path.data_memory[self.data_path.dar] = self.data_path.ddr
        self.tick()

    def dec(self):
        # Понижаем значение аккумулятор и переводим в буферный регистр
        self.data_path.alu.right_val = self.data_path.ddr
        self.data_path.alu.left_val = 0
        self.data_path.alu.incr_right = False
        self.data_path.alu.inverse_left = True
        self.data_path.alu.inverse_right = False
        self.data_path.alu.incr_left = False
        self.data_path.alu.carry = False
        self.data_path.alu.withFlags = True
        self.data_path.alu.operation = AluOp.SUM
        self.data_path.br = self.data_path.alu.alu_process()
        self.tick()

        # Кладём значение в ddr
        self.data_path.ddr = self.data_path.br
        self.tick()

        # Записываем значение в память данных
        self.data_path.data_memory[self.data_path.dar] = self.data_path.ddr
        self.tick()

    def hlt(self):
        raise HLTError()

    def execute_current_command(self):
        instr = self.data_path.ir
        operation = opcodes_by_value[((instr >> 56) & 0xFF)]
        self.executors[operation]()

    def interrupt(self):
        if self.is_interrupted:
            return

        if len(self.inputIrq) == 0:
            self.is_interrupted = False
            return

        if self.inputIrq[0][0] <= self.tickNum:
            self.is_interrupted = True

            # Кладём значение аккумулятора в стек и понижаем его
            self.data_path.alu.right_val = 0
            self.data_path.alu.left_val = self.data_path.acc
            self.data_path.alu.incr_right = False
            self.data_path.alu.inverse_left = False
            self.data_path.alu.inverse_right = False
            self.data_path.alu.incr_left = False
            self.data_path.alu.carry = False
            self.data_path.alu.withFlags = False
            self.data_path.alu.operation = AluOp.SUM
            self.data_path.idr = self.data_path.alu.alu_process()
            self.data_path.iar = self.data_path.sr

            self.tick()

            self.program[self.data_path.iar] = self.data_path.idr
            self.data_path.alu.right_val = self.data_path.sr
            self.data_path.alu.left_val = 0
            self.data_path.alu.incr_right = False
            self.data_path.alu.inverse_left = True
            self.data_path.alu.inverse_right = False
            self.data_path.alu.incr_left = False
            self.data_path.alu.carry = False
            self.data_path.alu.withFlags = False
            self.data_path.alu.operation = AluOp.SUM
            self.data_path.br = self.data_path.alu.alu_process()
            self.tick()

            self.data_path.sr = self.data_path.br
            self.tick()

            # Кладём значение адреса возврата в стек и понижаем его
            self.data_path.br = self.data_path.ip
            self.tick()

            # Загружаем адрес блока прерывания
            self.data_path.alu.right_val = self.intAddress
            self.data_path.alu.left_val = 0
            self.data_path.alu.incr_right = False
            self.data_path.alu.inverse_left = False
            self.data_path.alu.inverse_right = False
            self.data_path.alu.incr_left = False
            self.data_path.alu.carry = False
            self.data_path.alu.withFlags = False
            self.data_path.alu.operation = AluOp.SUM
            self.data_path.ip = self.data_path.alu.alu_process()

            self.tick()

            # Загружаем значение буферного регистра в idr, а указатель стека в iar
            self.data_path.iar = self.data_path.sr
            self.data_path.idr = self.data_path.br

            self.tick()

            # Сохраняем значение в памяти инструкций и уменьшаем значение sr
            self.program[self.data_path.iar] = self.data_path.idr
            self.data_path.alu.right_val = self.data_path.sr
            self.data_path.alu.left_val = 0
            self.data_path.alu.incr_right = False
            self.data_path.alu.inverse_left = True
            self.data_path.alu.inverse_right = False
            self.data_path.alu.incr_left = False
            self.data_path.alu.carry = False
            self.data_path.alu.withFlags = False
            self.data_path.alu.operation = AluOp.SUM
            self.data_path.br = self.data_path.alu.alu_process()
            self.tick()

            self.data_path.sr = self.data_path.br
            self.tick()

            # Кладём значение cостояния АЛУ в стек и понижаем его
            self.data_path.idr = self.data_path.alu.alu_state()
            self.data_path.iar = self.data_path.sr

            self.tick()

            self.program[self.data_path.iar] = self.data_path.idr
            self.data_path.alu.right_val = self.data_path.sr
            self.data_path.alu.left_val = 0
            self.data_path.alu.incr_right = False
            self.data_path.alu.inverse_left = True
            self.data_path.alu.inverse_right = False
            self.data_path.alu.incr_left = False
            self.data_path.alu.carry = False
            self.data_path.alu.withFlags = False
            self.data_path.alu.operation = AluOp.SUM
            self.data_path.br = self.data_path.alu.alu_process()
            self.tick()

            self.data_path.sr = self.data_path.br
            self.tick()

    def start(self):
        instructions_num = 0
        while True:
            try:
                self.instruction_fetch()
                self.data_address_fetch()
                self.execute_current_command()
                instructions_num += 1
                self.interrupt()
            except HLTError:
                logging.info(f"Instructions: {instructions_num}")
                logging.info(f"Ticks: {self.tickNum}")
                logging.info("End of program")
                break
            finally:
                logging.info(f"{self.print()}")

    def print(self):
        return f"ticks={self.tickNum}; {self.data_path.print()}; interrupt={self.is_interrupted}"


if __name__ == "__main__":
    logging.getLogger().setLevel(logging.DEBUG)
    parser = argparse.ArgumentParser(description="Execute binary executable file.")
    parser.add_argument("src", type=argparse.FileType("rb"), metavar="binary_file", help="binary executable file")
    parser.add_argument("inter", type=argparse.FileType("r"), metavar="interrupts")

    namespace = parser.parse_args()
    main(namespace.src, namespace.inter)
