## Отчет

* Марьин Савва Сергеевич, P33111
* `asm | acc | harv | hw | instr | binary | trap | mem | cstr | prob1`
* Без усложнения


#### Пояснение варианта

---

* `asm` -- синтаксис ассемблера. Необходима поддержка label-ов.

* `acc` -- Архитектура -- система команд должна быть выстроена вокруг аккумулятора.
  * Инструкции -- изменяют значение, хранимое в аккумуляторе.
  * Ввод-вывод осуществляется через аккумулятор.

* `harv` -- Архитектура организации памяти -- Гарвардская архитектура.

* `hw` -- Control Unit -- hardwired. Реализуется как часть модели.

* `instr` -- Точность модели -- процессор необходимо моделировать с точностью до каждой инструкции (наблюдается состояние после каждой инструкции).

* `binary` -- Представление машинного кода -- бинарное представление.
  * Требуются настоящие бинарные файлы, а не текстовые файлы с 0 и 1.
  * Требуется отладочный вывод в текстовый файл вида:
    ```text
    <address> - <HEXCODE> - <mnemonica> 
    20 - 03340301 - add #01 <- 34 + #03
    ```
    
* `trap` -- Прерывание
  * Ввод--вывод осуществляется токенами через систему прерываний. Логика работы:
    * при старте модели у вас есть расписание ввода ([(1, 'h'), (10, 'e'), (20, 'l'), (25, 'l'), (100, 'o')], где цифра -- момент поступления данных, символ -- токен);
    * процессор имеет систему прерываний:
      * прерывания считаем внутренними;
      * обработка прерывания должна приводить к вызову реализованного пользователем программного кода;
      * обработчик прерывания реализуется программистом на вашем языке;
    * в процессе моделирования идёт отсчёт времени в тактах, по наступлении события ввода -- происходит обработка прерывания;
    * из журнала работы процессора должно быть ясно, работаете вы в прерывании или нет;
    * вывод данных реализуется посимвольно, как в варианте stream, по выполнении команд в буфер вывода добавляется ещё один символ;
    * по окончании моделирования показать все выведенные данные;
    * ситуация наступления прерывания во время обработки прерывания должна быть проработана (способ -- на ваше усмотрение);
    * логика работы с буферами реализуется в рамках модели на Python;
    * не стоит путать "вызов прерывания" и "получение данных".

* `mem` -- Ввод-вывод ISA -- memory-mapped (порты ввода-вывода отображаются в память и доступ к ним осуществляется штатными командами),
  * отображение портов ввода-вывода в память должно конфигурироваться (можно hardcode-ом).

* `cstr` -- Поддержка строк -- Null-terminated (C string)

* `prob1` -- Алгоритм -- Multiples of 3 or 5. [Project Euler. Problem 1](https://projecteuler.net/problem=1)

---


### Язык программирования

Форма Бэкуса-Наура:

```ebnf
<программа> ::= 
        "section .data:" <перенос строки> <данные>
        "section .text:" <инструкции>
        
<данные> ::= <метка переменной> <переменная> <перенос строки>

<инструкции> ::= <строка_программы> | <строка_программы> <программа>
<строка_программы> ::= <адрес> | [<метка>] <адресная команда> <операнд> | 
[<метка>] <безадресная команда> | <пустая строка> 

<метка> ::= <слово> <перенос строки>
<адресная команда> = ld | sv | add | ... | inc | dec | (см. систему команд)
<безадресная команда> ::= ret | iret | hlt | (см. систему команд)
<операнд> ::= <число> | <метка>
<переменная> ::= <число> | <число> '<слово>'
<слово> ::= <символ> | <слово> <символ>
<число> ::= <цифра> | <число> <цифра>
<цифра> ::= 0| 1 | 2 | .. | 8 | 9
<символ> ::= a | b | c | ... | z | A | B | C | ... | Z | <цифра>
```

**Пояснение:**

Каждая непустая строка программы это одно из нижеперечисленных:

* **адресная команда**
    * может иметь метку перед собой
    * указывается название команды и адрес/значение операнда через пробел
* **безадресная команда**
    * может иметь метку перед собой
    * указывается только название команды
* **переменная**
    * указывается название переменной `val ` и значение
    * переменная может быть 16-битным знаковым числом
    * переменная может быть строкой: указывается строка в одинарных кавычках
* **адрес**
    * указывается название переменной
    * если требуется ввод или вывод с устройства, то можно использовать параметры INPUT, OUTPUT или OUTPUT_NUM в качестве адреса
    * может указываться название переменной с префиксом '~' для работы с адресом переменной
    * может указываться название переменной с префиксом '$' для получения значения по адресу в значении этой переменной

Пример программы, вычисляющей С = A + B

```asm
section .data:
  A 10
  B 33
section .text:
  _int:
      HLT
  _start:
      LD A
      ADD B
      SV OUTPUT_NUM
      HLT
```

**Семантика**

- Видимость данных -- глобальная
- Поддерживаются целочисленные литералы, находящиеся в диапазоне от $`-2^{31}`$ до $`2^{31}-1`$
- Поддерживаются строковые литералы, символы стоки необходимо заключить в одинарные кавычки
- Код выполняется последовательно

- Программа обязательно должна включать метку `_start`, указывающую на 1-ю выполняемую инструкцию. Эта метка не может
  указывать на константу.
- Программа обязательно должна включать метку `_int`, указывающую на функцию обработки прерываний. Эта метка не может
  указывать на константу.
- Метки находятся до строки инструкции, операнды находятся на одной строке с командами.
- Пустые строки игнорируются, количество пробелов в начале и конце строки не влияет на работу программы.

Память выделяется статически, при запуске модели.


## Организация памяти

* Память команд и данныx --- раздельна
* Размер машинного слова --- `64` бита
* Память содержит `1024` ячейки
* Адрес `1023` является указателем стека при старте процессора. Стек растет вверх.
* Ячейка с адресом `5555` маппится на устройство ввода
* Ячейки с адресами `5556 и 5557` маппятся на устройство вывода


* Поддерживаются следующие **виды адресаций**:
    * **Прямая**: в качестве аргумента команды передается число, значение которого будет использовано как
      операнд.
      Например, команда `add 30` обозначает, что к значению в аккумуляторе добавится число 30.

    * **Косвенная**: в качестве аргумента команды передается переменная, по которой лежит значение операнда.
      Например, если `value лежит по адресу 10 и mem[10] = 25`, то команда `add value` также обозначает, что к аккумулятору
      добавится значение 25.

    * **Относительная**: в качестве аргумента команды передается адрес, по которому лежит адрес операнда.
      Например, если `value лежит по адресу 10 и mem[10] = 25 c mem[25] = 40`, то команда `add $value` обозначает, что к аккумулятору
      добавится значение 40.

* Существует несколько **регистров**:
    * Аккумулятор (ACC): в него записываются результаты операций
    * Счетчик команд (IP): хранит адрес следующей выполняемой команды
    * Буферный регистр (BR): хранит промежуточные значения
    * Регистр данных для записи в память данных (DDR): хранит значение получаемое/записываемое из/в память данных
    * Регистр данных для записи в память инструкций (IDR): хранит значение получаемое/записываемое из/в память инструкций 
    * Регистр адреса для записи в память данных (DAR): хранит адрес по которому получают/записывают значение из/в память данных
    * Регистр адреса для записи в память инструкций (IAR): хранит адрес по которому получают/записывают значение из/в память инструкций
    * Регистр текущей инструкции (IR): хранит выполняемую инструкцию
    * Указатель стека (SR): Указывает на вершину стека
    * Регистр состояния (PS): хранит маркеры состояния АЛУ и прерываний

## Система команд

Особенности процессора:

- Машинное слово -- `64` бита, знаковое.
- В качестве аргументов команды принимают `48` битные беззнаковые адреса

Каждая команда выполняется в несколько циклов:

1. Цикл выборки инструкции: по адресу из счетчика команд из памяти инструкции достается команда

- `IP -> IAR, IP + 1 -> IP, mem[IAR] -> IDR, IDR -> IR`

2. Цикл выборки адреса инструкции (для адресных команд): в регистр данных помещается адрес операнда, регистр данных передается в
   регистр адреса, из памяти в регистр данных записывается значение операнда

- `IR[addr] -> DAR, mem[DAR] -> DDR`

3. Цикл исполнения: совершаются действия, необходимые для выполнения команды. Результаты вычисления записываются в
   аккумулятор, устройство вывода или память
4. Цикл прерывания: проверяется, не произошел ли запрос на прерывание

### Набор инструкций

| Команда | Адресная | Описание                                                                          |
|:--------|:---------|:----------------------------------------------------------------------------------|
| ld      | +        | загрузить значение из заданной ячейки                                             |
| sv      | +        | загрузить значение в заданную ячейку                                              |
| add     | +        | добавить значение из заданной ячейки к аккумулятору                               |
| sub     | +        | вычесть значение из заданной ячейки из аккумулятора                               |
| cmp     | +        | вычесть значение из заданной ячейки и установить флаги без загрузки в аккумулятор |
| jmp     | +        | перейти в заданную ячейку                                                         |
| je      | +        | перейти в заданную ячейку если Z = 1                                              |
| jne     | +        | перейти в заданную ячейку если Z = 0                                              |
| call    | +        | перейти в заданную ячейку с сохранением адреса возврата                           |
| ret     | -        | вернуться из подпрограммы                                                         |
| mul     | +        | умножить значение из заданной ячейки на аккумулятор                               |
| div     | +        | поделить аккумулятор на значение из заданной ячейки                               |
| inc     | +        | увеличить значение в заданной ячейке на 1 без записи в аккумулятор                |
| dec     | +        | уменьшить значение в заданной ячейке на 1 без записи в аккумулятор                |
| iret    | -        | возврат из прерывания                                                             |
| hlt     | -        | остановка программы                                                               |

### Кодирование инструкций

- Код сериализуется в бинарный вид.
- В бинарнике разделенно хранятся данные, инструкции и указатели на точку старта программы и прерывания

## Транслятор

Интерфейс командной строки: `translator.py <input_file> --output <output-file>`

Реализовано в модуле: [translator](./translator.py)

Этапы трансляции (функция `translate`):

1. Чтение переменных в секции '.data' и определение их адресов в памяти
2. Парсинг кода построчно, определение типа команды (адресная, безадресная, константа). 
3. Проверка на соответствие и повтор функций и переменных.
4. Генерация машинного кода и мнемоники в зависимости от типа команды

Правила генерации машинного кода:

- Метки не сохраняются в машинном коде, используется прямой адрес. Данные и инструкции преобразуются в бинарный вид. 

## Модель процессора

Интерфейс командной строки: `machine.py <machine_code_file> <interruptions-file>`

Реализовано в модуле: [machine](./machine.py).

#### Data path

![Proc](./proc.jpg)

#### Взаимодействие с памятью данных

![Data memory](./data_memory.jpg)

Реализован в классе `DataPath`.

Возможно изменять состояние машины через поля. Внутри модели также используется класс `ALU` для управления состоянием АЛУ 
и получения его данных.

#### ControlUnit (взаимодействие с памятью инструкций)

![Control unit](./control_unit.jpg)

Реализован в классе `ControlUnit`.

- Метод `start` начинает моделирование процесса, исполняя 4 цикла: выборки команд, данных, исполнения и прерывания.
- В случае прерывания сохраняются значения регистров acc, sp и ip в стек, который находится в памяти команд

Особенности работы модели:

- Шаг моделирования соответствует одной исполненной команде с выводом состояния в журнал.
- Для журнала состояний процессора используется стандартный модуль `logging`.
- Память модели конфигурируема, только стоит не забыть переопределить переменные io.
- Остановка моделирования осуществляется при:
    - Ошибки логики команд;
    - если выполнена инструкция `hlt`.

Проверка наличия запроса прерывания осуществляется после выполнения текущей команды. Проверяется, находится ли 
модель уже в прерывании и пришло ли прерывание.

- Вложенные прерывания невозможны, пока не обработается текущее прерывание, не начнется и следующее

### Тестирование

Для тестирования выбраны 7 алгоритмов:
1. [cat](./golden/test_cat.yml) ([source](./programs/cat))
2. [hello](./golden/test_hello.yml) ([source](./programs/hello))
3. [dialog](./golden/test_dialog.yml) ([source](./programs/dialog))
4. [prob1](./golden/test_prob1.yml) ([source](./programs/prob1))

Golden-тесты реализованы в [integration_test.py](integration_test.py), конфигурация к ним 
находится в директории [golden](./golden).

CI:

```yaml
lab3-savasm:
  stage: test
  image:
    name: ryukzak/python-tools
    entrypoint: [""]
  script:
    - poetry install
    - coverage run -m pytest --verbose
    - find . -type f -name "*.py" | xargs -t coverage report
    - ruff format --diff .
    - ruff check .
```

где:
* `ryukzak/python-tools` - docker образ содержит все необходимые для проверки утилиты
* `poetry` - управления зависимостями
* `coverage` - формирование отчёта об уровне покрытия исходного кода
* `pytest` - утилита для запуска тестов
* `ruff` - утилита для форматирования и проверки стиля кодирования

Пример использования и журнал работы процессора на примере `hello`:
``` shell
$ cat programs/hello
section .data:
    counter 0
    name 'Hello world!'
section .text:
    _int:
        HLT
    _start:
        LD ~name
        SV counter
    _write:
        LD $counter
        CMP 0
        JE _end
        SV OUTPUT
        INC counter
        JMP _write
    _end:
        HLT
$ cat inter.json
[]
$ ./translator.py programs/hello output
LoC = 18
bytes = 214
instr = 10
$ ./machine.py output inter.json
INFO:root:ticks=5; alu_state=0000; acc=0x1; ip=0x2; br=0x0; ddr=0x1; idr=0x101000000000001; iar=0x1; dar=0x0; id=0x101000000000001; sr=0x3ff; interrupt=False
INFO:root:ticks=11; alu_state=0000; acc=0x1; ip=0x3; br=0x0; ddr=0x0; idr=0x202000000000000; iar=0x2; dar=0x0; id=0x202000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=19; alu_state=0000; acc=0x48; ip=0x4; br=0x0; ddr=0x48; idr=0x103000000000000; iar=0x3; dar=0x1; id=0x103000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=24; alu_state=0000; acc=0x48; ip=0x5; br=0x0; ddr=0x0; idr=0x501000000000000; iar=0x4; dar=0x1; id=0x501000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=27; alu_state=0000; acc=0x48; ip=0x6; br=0x0; ddr=0x0; idr=0x704000000000009; iar=0x5; dar=0x1; id=0x704000000000009; sr=0x3ff; interrupt=False
INFO:root:ticks=32; alu_state=0000; acc=0x48; ip=0x7; br=0x0; ddr=0x15b4; idr=0x2010000000015b4; iar=0x6; dar=0x1; id=0x2010000000015b4; sr=0x3ff; interrupt=False
INFO:root:ticks=40; alu_state=0000; acc=0x48; ip=0x8; br=0x2; ddr=0x2; idr=0xd02000000000000; iar=0x7; dar=0x0; id=0xd02000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=44; alu_state=0000; acc=0x48; ip=0x3; br=0x2; ddr=0x2; idr=0x604000000000003; iar=0x8; dar=0x0; id=0x604000000000003; sr=0x3ff; interrupt=False
INFO:root:ticks=52; alu_state=0000; acc=0x65; ip=0x4; br=0x2; ddr=0x65; idr=0x103000000000000; iar=0x3; dar=0x2; id=0x103000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=57; alu_state=0000; acc=0x65; ip=0x5; br=0x2; ddr=0x0; idr=0x501000000000000; iar=0x4; dar=0x2; id=0x501000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=60; alu_state=0000; acc=0x65; ip=0x6; br=0x2; ddr=0x0; idr=0x704000000000009; iar=0x5; dar=0x2; id=0x704000000000009; sr=0x3ff; interrupt=False
INFO:root:ticks=65; alu_state=0000; acc=0x65; ip=0x7; br=0x2; ddr=0x15b4; idr=0x2010000000015b4; iar=0x6; dar=0x2; id=0x2010000000015b4; sr=0x3ff; interrupt=False
INFO:root:ticks=73; alu_state=0000; acc=0x65; ip=0x8; br=0x3; ddr=0x3; idr=0xd02000000000000; iar=0x7; dar=0x0; id=0xd02000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=77; alu_state=0000; acc=0x65; ip=0x3; br=0x3; ddr=0x3; idr=0x604000000000003; iar=0x8; dar=0x0; id=0x604000000000003; sr=0x3ff; interrupt=False
INFO:root:ticks=85; alu_state=0000; acc=0x6c; ip=0x4; br=0x3; ddr=0x6c; idr=0x103000000000000; iar=0x3; dar=0x3; id=0x103000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=90; alu_state=0000; acc=0x6c; ip=0x5; br=0x3; ddr=0x0; idr=0x501000000000000; iar=0x4; dar=0x3; id=0x501000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=93; alu_state=0000; acc=0x6c; ip=0x6; br=0x3; ddr=0x0; idr=0x704000000000009; iar=0x5; dar=0x3; id=0x704000000000009; sr=0x3ff; interrupt=False
INFO:root:ticks=98; alu_state=0000; acc=0x6c; ip=0x7; br=0x3; ddr=0x15b4; idr=0x2010000000015b4; iar=0x6; dar=0x3; id=0x2010000000015b4; sr=0x3ff; interrupt=False
INFO:root:ticks=106; alu_state=0000; acc=0x6c; ip=0x8; br=0x4; ddr=0x4; idr=0xd02000000000000; iar=0x7; dar=0x0; id=0xd02000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=110; alu_state=0000; acc=0x6c; ip=0x3; br=0x4; ddr=0x4; idr=0x604000000000003; iar=0x8; dar=0x0; id=0x604000000000003; sr=0x3ff; interrupt=False
INFO:root:ticks=118; alu_state=0000; acc=0x6c; ip=0x4; br=0x4; ddr=0x6c; idr=0x103000000000000; iar=0x3; dar=0x4; id=0x103000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=123; alu_state=0000; acc=0x6c; ip=0x5; br=0x4; ddr=0x0; idr=0x501000000000000; iar=0x4; dar=0x4; id=0x501000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=126; alu_state=0000; acc=0x6c; ip=0x6; br=0x4; ddr=0x0; idr=0x704000000000009; iar=0x5; dar=0x4; id=0x704000000000009; sr=0x3ff; interrupt=False
INFO:root:ticks=131; alu_state=0000; acc=0x6c; ip=0x7; br=0x4; ddr=0x15b4; idr=0x2010000000015b4; iar=0x6; dar=0x4; id=0x2010000000015b4; sr=0x3ff; interrupt=False
INFO:root:ticks=139; alu_state=0000; acc=0x6c; ip=0x8; br=0x5; ddr=0x5; idr=0xd02000000000000; iar=0x7; dar=0x0; id=0xd02000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=143; alu_state=0000; acc=0x6c; ip=0x3; br=0x5; ddr=0x5; idr=0x604000000000003; iar=0x8; dar=0x0; id=0x604000000000003; sr=0x3ff; interrupt=False
INFO:root:ticks=151; alu_state=0000; acc=0x6f; ip=0x4; br=0x5; ddr=0x6f; idr=0x103000000000000; iar=0x3; dar=0x5; id=0x103000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=156; alu_state=0000; acc=0x6f; ip=0x5; br=0x5; ddr=0x0; idr=0x501000000000000; iar=0x4; dar=0x5; id=0x501000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=159; alu_state=0000; acc=0x6f; ip=0x6; br=0x5; ddr=0x0; idr=0x704000000000009; iar=0x5; dar=0x5; id=0x704000000000009; sr=0x3ff; interrupt=False
INFO:root:ticks=164; alu_state=0000; acc=0x6f; ip=0x7; br=0x5; ddr=0x15b4; idr=0x2010000000015b4; iar=0x6; dar=0x5; id=0x2010000000015b4; sr=0x3ff; interrupt=False
INFO:root:ticks=172; alu_state=0000; acc=0x6f; ip=0x8; br=0x6; ddr=0x6; idr=0xd02000000000000; iar=0x7; dar=0x0; id=0xd02000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=176; alu_state=0000; acc=0x6f; ip=0x3; br=0x6; ddr=0x6; idr=0x604000000000003; iar=0x8; dar=0x0; id=0x604000000000003; sr=0x3ff; interrupt=False
INFO:root:ticks=184; alu_state=0000; acc=0x20; ip=0x4; br=0x6; ddr=0x20; idr=0x103000000000000; iar=0x3; dar=0x6; id=0x103000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=189; alu_state=0000; acc=0x20; ip=0x5; br=0x6; ddr=0x0; idr=0x501000000000000; iar=0x4; dar=0x6; id=0x501000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=192; alu_state=0000; acc=0x20; ip=0x6; br=0x6; ddr=0x0; idr=0x704000000000009; iar=0x5; dar=0x6; id=0x704000000000009; sr=0x3ff; interrupt=False
INFO:root:ticks=197; alu_state=0000; acc=0x20; ip=0x7; br=0x6; ddr=0x15b4; idr=0x2010000000015b4; iar=0x6; dar=0x6; id=0x2010000000015b4; sr=0x3ff; interrupt=False
INFO:root:ticks=205; alu_state=0000; acc=0x20; ip=0x8; br=0x7; ddr=0x7; idr=0xd02000000000000; iar=0x7; dar=0x0; id=0xd02000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=209; alu_state=0000; acc=0x20; ip=0x3; br=0x7; ddr=0x7; idr=0x604000000000003; iar=0x8; dar=0x0; id=0x604000000000003; sr=0x3ff; interrupt=False
INFO:root:ticks=217; alu_state=0000; acc=0x77; ip=0x4; br=0x7; ddr=0x77; idr=0x103000000000000; iar=0x3; dar=0x7; id=0x103000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=222; alu_state=0000; acc=0x77; ip=0x5; br=0x7; ddr=0x0; idr=0x501000000000000; iar=0x4; dar=0x7; id=0x501000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=225; alu_state=0000; acc=0x77; ip=0x6; br=0x7; ddr=0x0; idr=0x704000000000009; iar=0x5; dar=0x7; id=0x704000000000009; sr=0x3ff; interrupt=False
INFO:root:ticks=230; alu_state=0000; acc=0x77; ip=0x7; br=0x7; ddr=0x15b4; idr=0x2010000000015b4; iar=0x6; dar=0x7; id=0x2010000000015b4; sr=0x3ff; interrupt=False
INFO:root:ticks=238; alu_state=0000; acc=0x77; ip=0x8; br=0x8; ddr=0x8; idr=0xd02000000000000; iar=0x7; dar=0x0; id=0xd02000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=242; alu_state=0000; acc=0x77; ip=0x3; br=0x8; ddr=0x8; idr=0x604000000000003; iar=0x8; dar=0x0; id=0x604000000000003; sr=0x3ff; interrupt=False
INFO:root:ticks=250; alu_state=0000; acc=0x6f; ip=0x4; br=0x8; ddr=0x6f; idr=0x103000000000000; iar=0x3; dar=0x8; id=0x103000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=255; alu_state=0000; acc=0x6f; ip=0x5; br=0x8; ddr=0x0; idr=0x501000000000000; iar=0x4; dar=0x8; id=0x501000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=258; alu_state=0000; acc=0x6f; ip=0x6; br=0x8; ddr=0x0; idr=0x704000000000009; iar=0x5; dar=0x8; id=0x704000000000009; sr=0x3ff; interrupt=False
INFO:root:ticks=263; alu_state=0000; acc=0x6f; ip=0x7; br=0x8; ddr=0x15b4; idr=0x2010000000015b4; iar=0x6; dar=0x8; id=0x2010000000015b4; sr=0x3ff; interrupt=False
INFO:root:ticks=271; alu_state=0000; acc=0x6f; ip=0x8; br=0x9; ddr=0x9; idr=0xd02000000000000; iar=0x7; dar=0x0; id=0xd02000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=275; alu_state=0000; acc=0x6f; ip=0x3; br=0x9; ddr=0x9; idr=0x604000000000003; iar=0x8; dar=0x0; id=0x604000000000003; sr=0x3ff; interrupt=False
INFO:root:ticks=283; alu_state=0000; acc=0x72; ip=0x4; br=0x9; ddr=0x72; idr=0x103000000000000; iar=0x3; dar=0x9; id=0x103000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=288; alu_state=0000; acc=0x72; ip=0x5; br=0x9; ddr=0x0; idr=0x501000000000000; iar=0x4; dar=0x9; id=0x501000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=291; alu_state=0000; acc=0x72; ip=0x6; br=0x9; ddr=0x0; idr=0x704000000000009; iar=0x5; dar=0x9; id=0x704000000000009; sr=0x3ff; interrupt=False
INFO:root:ticks=296; alu_state=0000; acc=0x72; ip=0x7; br=0x9; ddr=0x15b4; idr=0x2010000000015b4; iar=0x6; dar=0x9; id=0x2010000000015b4; sr=0x3ff; interrupt=False
INFO:root:ticks=304; alu_state=0000; acc=0x72; ip=0x8; br=0xa; ddr=0xa; idr=0xd02000000000000; iar=0x7; dar=0x0; id=0xd02000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=308; alu_state=0000; acc=0x72; ip=0x3; br=0xa; ddr=0xa; idr=0x604000000000003; iar=0x8; dar=0x0; id=0x604000000000003; sr=0x3ff; interrupt=False
INFO:root:ticks=316; alu_state=0000; acc=0x6c; ip=0x4; br=0xa; ddr=0x6c; idr=0x103000000000000; iar=0x3; dar=0xa; id=0x103000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=321; alu_state=0000; acc=0x6c; ip=0x5; br=0xa; ddr=0x0; idr=0x501000000000000; iar=0x4; dar=0xa; id=0x501000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=324; alu_state=0000; acc=0x6c; ip=0x6; br=0xa; ddr=0x0; idr=0x704000000000009; iar=0x5; dar=0xa; id=0x704000000000009; sr=0x3ff; interrupt=False
INFO:root:ticks=329; alu_state=0000; acc=0x6c; ip=0x7; br=0xa; ddr=0x15b4; idr=0x2010000000015b4; iar=0x6; dar=0xa; id=0x2010000000015b4; sr=0x3ff; interrupt=False
INFO:root:ticks=337; alu_state=0000; acc=0x6c; ip=0x8; br=0xb; ddr=0xb; idr=0xd02000000000000; iar=0x7; dar=0x0; id=0xd02000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=341; alu_state=0000; acc=0x6c; ip=0x3; br=0xb; ddr=0xb; idr=0x604000000000003; iar=0x8; dar=0x0; id=0x604000000000003; sr=0x3ff; interrupt=False
INFO:root:ticks=349; alu_state=0000; acc=0x64; ip=0x4; br=0xb; ddr=0x64; idr=0x103000000000000; iar=0x3; dar=0xb; id=0x103000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=354; alu_state=0000; acc=0x64; ip=0x5; br=0xb; ddr=0x0; idr=0x501000000000000; iar=0x4; dar=0xb; id=0x501000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=357; alu_state=0000; acc=0x64; ip=0x6; br=0xb; ddr=0x0; idr=0x704000000000009; iar=0x5; dar=0xb; id=0x704000000000009; sr=0x3ff; interrupt=False
INFO:root:ticks=362; alu_state=0000; acc=0x64; ip=0x7; br=0xb; ddr=0x15b4; idr=0x2010000000015b4; iar=0x6; dar=0xb; id=0x2010000000015b4; sr=0x3ff; interrupt=False
INFO:root:ticks=370; alu_state=0000; acc=0x64; ip=0x8; br=0xc; ddr=0xc; idr=0xd02000000000000; iar=0x7; dar=0x0; id=0xd02000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=374; alu_state=0000; acc=0x64; ip=0x3; br=0xc; ddr=0xc; idr=0x604000000000003; iar=0x8; dar=0x0; id=0x604000000000003; sr=0x3ff; interrupt=False
INFO:root:ticks=382; alu_state=0000; acc=0x21; ip=0x4; br=0xc; ddr=0x21; idr=0x103000000000000; iar=0x3; dar=0xc; id=0x103000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=387; alu_state=0000; acc=0x21; ip=0x5; br=0xc; ddr=0x0; idr=0x501000000000000; iar=0x4; dar=0xc; id=0x501000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=390; alu_state=0000; acc=0x21; ip=0x6; br=0xc; ddr=0x0; idr=0x704000000000009; iar=0x5; dar=0xc; id=0x704000000000009; sr=0x3ff; interrupt=False
INFO:root:ticks=395; alu_state=0000; acc=0x21; ip=0x7; br=0xc; ddr=0x15b4; idr=0x2010000000015b4; iar=0x6; dar=0xc; id=0x2010000000015b4; sr=0x3ff; interrupt=False
INFO:root:ticks=403; alu_state=0000; acc=0x21; ip=0x8; br=0xd; ddr=0xd; idr=0xd02000000000000; iar=0x7; dar=0x0; id=0xd02000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=407; alu_state=0000; acc=0x21; ip=0x3; br=0xd; ddr=0xd; idr=0x604000000000003; iar=0x8; dar=0x0; id=0x604000000000003; sr=0x3ff; interrupt=False
INFO:root:ticks=415; alu_state=0000; acc=0x0; ip=0x4; br=0xd; ddr=0x0; idr=0x103000000000000; iar=0x3; dar=0xd; id=0x103000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=420; alu_state=0100; acc=0x0; ip=0x5; br=0xd; ddr=0x0; idr=0x501000000000000; iar=0x4; dar=0xd; id=0x501000000000000; sr=0x3ff; interrupt=False
INFO:root:ticks=424; alu_state=0100; acc=0x0; ip=0x9; br=0xd; ddr=0x0; idr=0x704000000000009; iar=0x5; dar=0xd; id=0x704000000000009; sr=0x3ff; interrupt=False
INFO:root:Instructions: 77
INFO:root:Ticks: 427
INFO:root:End of program
INFO:root:ticks=427; alu_state=0100; acc=0x0; ip=0xa; br=0xd; ddr=0x0; idr=0x6304000000000000; iar=0x9; dar=0xd; id=0x6304000000000000; sr=0x3ff; interrupt=False
$ ./cat result.txt
Hello world!
```

Пример проверки исходного кода:

``` shell
$ poetry run pytest . -v --update-goldens
Testing started at 20:18 ...
Launching pytest with arguments intergration_test.py::test_whole_by_golden --no-header --no-summary -q in /Users/masasdf/itmo/ca/lab3

============================= test session starts ==============================
collecting ... collected 4 items

intergration_test.py::test_whole_by_golden[golden/test_cat.yml] 
intergration_test.py::test_whole_by_golden[golden/test_dialog.yml] 
intergration_test.py::test_whole_by_golden[golden/test_prob1.yml] 
intergration_test.py::test_whole_by_golden[golden/test_hello.yml] 

============================== 4 passed in 0.54s ===============================

Process finished with exit code 0
PASSED   [ 25%]PASSED [ 50%]PASSED [ 75%]PASSED [100%]

$ poetry run ruff format .         
8 files left unchanged
```

Статистика:

| alg    | LoC | code byte | code instr | debug lines | instr | ticks |
|--------|-----|-----------|------------|-------------|-------|-------|
| cat    | 14  | 94        | 9          | 13          | 40    | 310   |
| hello  | 18  | 214       | 10         | 16          | 77    | 427   |
| dialog | 60  | 646       | 41         | 54          | 321   | 1766  |
| prob1  | 39  | 262       | 27         | 34          | 4871  | 27759 |