import contextlib
import io
import os
import tempfile

import machine
import pytest
import translator


@pytest.mark.golden_test("golden/*.yml")
def test_whole_by_golden(golden):
    # Временная папка для теста
    with tempfile.TemporaryDirectory() as tmpdirname:
        # Готовим имена файлов для входных и выходных данных.
        source = os.path.join(tmpdirname, "source.asm")
        output_bin = os.path.join(tmpdirname, "output.bin")
        interrupt_json = os.path.join(tmpdirname, "interrupt.json")

        # Записываем входные данные в файлы. Данные берутся из теста.
        with open(source, "w", encoding="utf-8") as file:
            file.write(golden["source"])
        with open(interrupt_json, "w", encoding="utf-8") as file:
            file.write(golden["interrupt"])

        with contextlib.redirect_stdout(io.StringIO()):
            with open(source, encoding="utf-8") as s, open(output_bin, "wb") as b:
                translator.process(s, b)

            with open("debug.txt", encoding="utf-8") as d:
                assert d.read().strip() == golden.out["debug"]

            with open(output_bin, "rb") as b, open(interrupt_json, encoding="utf-8") as i:
                machine.main(b, i)

            with open("result.txt", encoding="utf-8") as r:
                assert r.read() == golden.out["output"]
