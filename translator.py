import argparse

from ParseExceptionError import ParseExceptionError
from UnknownBlockError import UnknownBlockError
from utils import INPUT, OUTPUT, OUTPUT_NUM, AddressType, Operation, OperationType, opcodes_by_name

BIT_DEPTH = 64


def parse_asm_instructions(code: str):
    rows = code.split("\n")
    rows_num = 0
    settings = {}
    functions = []
    data = []
    instructions = []
    current_block_type = None
    data_num = 0
    for row in rows:
        if row.strip() == "":
            continue
        rows_num += 1
        words = row.strip().split(" ")
        old_block_type = current_block_type

        current_block_type = choose_block_type(words, old_block_type)

        if current_block_type != old_block_type:
            continue

        if current_block_type == "DATA":
            block = parse_data_block(row, data)
            block["num"] = data_num

            if isinstance(block["value"], int):
                data_num += 1
            else:
                data_num += len(block["value"]) + 1
            data.append(block)

        elif current_block_type == "TEXT":
            if words[-1][-1] == ":":
                current_function = parse_func(words, functions)

                functions.append({"func": current_function, "num": None})
            elif len(words) == 2:
                command = opcodes_by_name[words[0].upper()]
                arg = words[-1]
                command_block = parse_arg_command(command, arg)

                instructions.append(command_block)
                calculate_new_inst_num(functions, instructions)
            elif len(words) == 1:
                command = opcodes_by_name[words[0].upper()]
                command_block = parse_single_command(command)
                instructions.append(command_block)

                calculate_new_inst_num(functions, instructions)
            else:
                raise ParseExceptionError
        else:
            raise ParseExceptionError

    normailize(instructions, data, functions)

    print(f"LoC = {rows_num}")
    settings["start"] = dict((f["func"], f["num"]) for f in functions)["_start"]
    settings["interrupt"] = dict((f["func"], f["num"]) for f in functions)["_int"]
    return [{f"{d['num']}": d["value"]} for d in data], instructions, settings


def normailize(instructions, data, functions):
    for instr in instructions:
        if instr["command"].type == OperationType.ARG_DATA and instr["addrType"] != AddressType.STRAIGHT:
            arg = instr["arg"]
            instr = parse_address_type(data, arg, instr)

        if instr["command"].type == OperationType.ARG_JMP:
            changed = False
            arg = instr["arg"]
            for fun in functions:
                if arg == fun["func"]:
                    instr["arg"] = fun["num"]
                    changed = True
                    break
            if not changed:
                raise ParseExceptionError

        instr["addrType"] = instr["addrType"]
        instr["command"] = instr["command"]


def parse_address_type(data, arg, instr):
    changed = False

    if arg[0] == "~":
        for d in data:
            if arg[1:] == d["name"]:
                instr["arg"] = d["num"]
                instr["addrType"] = AddressType.STRAIGHT
                changed = True
                break
    elif arg[0] == "$":
        for d in data:
            if arg[1:] == d["name"]:
                instr["arg"] = d["num"]
                instr["addrType"] = AddressType.RELATIVE_INDIRECT
                changed = True
                break
    else:
        for d in data:
            if arg == d["name"]:
                instr["arg"] = d["num"]
                changed = True
                break

    if not changed:
        raise ParseExceptionError

    return instr


def choose_block_type(words, current_block_type):
    if words[0] == "section" and words[-1][-1] == ":":
        if words[-1] == ".data:":
            return "DATA"
        if words[-1] == ".text:":
            return "TEXT"

        raise UnknownBlockError

    return current_block_type


def parse_data_block(row, data):
    words = row.strip().split(" ", 1)
    if words[1].isdigit():
        value = int(words[1])
    else:
        value = words[1].strip()
        if value[0] != "'" or value[-1] != "'":
            raise ParseExceptionError
        value = value[1:-1]

    name = words[0].strip()
    if name in [d["name"] for d in data]:
        raise ParseExceptionError
    block = {}
    block["name"] = name
    block["value"] = value

    return block


def parse_func(words, functions):
    current_function = words[0][:-1]
    if current_function in [f["func"] for f in functions]:
        raise ParseExceptionError

    return current_function


def parse_arg_command(command, arg):
    if command.type == OperationType.SINGLE:
        raise ParseExceptionError

    command_block = {}
    command_block["command"] = command
    if command == Operation.SV and arg == "OUTPUT":
        command_block["arg"] = OUTPUT
        command_block["addrType"] = AddressType.STRAIGHT
    elif command == Operation.SV and arg == "OUTPUT_NUM":
        command_block["arg"] = OUTPUT_NUM
        command_block["addrType"] = AddressType.STRAIGHT
    elif command == Operation.LD and arg == "INPUT":
        command_block["arg"] = INPUT
        command_block["addrType"] = AddressType.STRAIGHT
    else:
        command_block["arg"] = arg
        if arg.isdigit():
            command_block["addrType"] = AddressType.STRAIGHT
            command_block["arg"] = int(arg)
        elif command.type == OperationType.ARG_JMP:
            command_block["addrType"] = AddressType.WITHOUT
        elif command.type == OperationType.ARG_DATA:
            command_block["addrType"] = AddressType.INDIRECT_DATA

    return command_block


def parse_single_command(command):
    command_block = {}
    if command.type != OperationType.SINGLE:
        raise ParseExceptionError
    command_block["command"] = command
    command_block["arg"] = 0
    command_block["addrType"] = AddressType.WITHOUT

    return command_block


def calculate_new_inst_num(functions, instructions):
    if len(functions) != 0 and functions[-1]["num"] is None:
        functions[-1]["num"] = len(instructions) - 1


def int_to_bytes(i: int, size: int) -> bytearray:
    if i not in range(-(2 ** (BIT_DEPTH - 1)), 2 ** (BIT_DEPTH - 1) - 1):
        raise ParseExceptionError

    binary = []
    while size > 0:
        binary.append(i & 0xFF)
        i >>= 8
        size -= 1

    return bytearray(binary[::-1])


def write_data_memory(data):
    binary = bytearray()
    for arg in data:
        value = next(iter(arg.values()))
        if isinstance(value, int):
            binary.extend(int_to_bytes(value, 8))
        else:
            value = value + "\0"
            for word in value:
                binary.extend(int_to_bytes(ord(word), 8))
    return binary


def write_instr_memory(instr):
    binary = bytearray()
    for block in instr:
        binary.extend(construct_inst_binary(block))
    return binary


def construct_inst_binary(block):
    binary = bytearray()
    binary.extend(int_to_bytes(block["command"].number, 1))
    binary.extend(int_to_bytes(block["addrType"].value, 1))
    binary.extend(int_to_bytes(block["arg"], 6))

    return binary


def write_settings(settings):
    binary = bytearray()

    binary.extend(int_to_bytes(settings["start"], 4))
    binary.extend(int_to_bytes(settings["interrupt"], 4))

    return binary


def code_to_binary(data, instructions, settings):
    magic = 0xC0DE
    base_offset = 2 + 2 + 2 + 2
    data_binary = write_data_memory(data)
    data_length = len(data_binary)
    data_offset = base_offset
    instr_binary = write_instr_memory(instructions)
    instr_length = len(instr_binary)
    instr_offset = base_offset + 2 + data_length
    settings_binary = write_settings(settings)
    settings_length = len(settings_binary)
    settings_offset = base_offset + 2 + data_length + 2 + instr_length
    binary = bytearray()
    binary.extend(int_to_bytes(data_offset, 2))
    binary.extend(int_to_bytes(instr_offset, 2))
    binary.extend(int_to_bytes(settings_offset, 2))
    binary.extend(int_to_bytes(magic, 2))
    binary.extend(int_to_bytes(data_length, 2))
    binary.extend(data_binary)
    binary.extend(int_to_bytes(instr_length, 2))
    binary.extend(instr_binary)
    binary.extend(int_to_bytes(settings_length, 2))
    binary.extend(settings_binary)

    return binary


def code_to_mnemonic(data, instructions, settings):
    lines = ["##### Data memory #####"]
    lines.extend(text_data_memory(data))
    lines.append("\n##### Instruction memory #####")
    lines.extend(text_instr_memory(instructions, settings))
    print(f"debug = {len(lines)}")

    return "\n".join(lines).expandtabs(15)


def text_data_memory(data):
    lines = ["<address>\t<length>\t<data>"]
    for block in data:
        address = next(iter(block.keys()))
        value = block[address]
        address = "0x{:08x}".format(int(address))
        if isinstance(value, int):
            lines.append(f"{address}\t\t{value}")
        else:
            lines.append(f"{address}\t{len(value) + 1}\t{value!s}")

    return lines


def text_instr_memory(instructions, settings):
    lines = [f'<address>\t{"hexcode".ljust(18)}\t{"<mnemonica>".ljust(30)}\t<settings>']
    start = settings["start"]
    interrupt = settings["interrupt"]

    for i in range(len(instructions)):
        address = "0x{:08x}".format(i)
        hexcode = construct_inst_binary(instructions[i]).hex()
        mnemonic = create_mnemonic(instructions[i])
        setting = ""
        if i == start:
            setting = "start"
        if i == interrupt:
            setting = "interrupt"
        lines.append(f"{address}\t0x{hexcode}\t{mnemonic.ljust(30)}\t{setting}")

    return lines


def create_mnemonic(instr):
    result = ""
    command = instr["command"]
    address_type = instr["addrType"]
    mnemonic_format = command.mnemonic_format
    if command.type == OperationType.SINGLE:
        result = mnemonic_format
    elif command.type == OperationType.ARG_DATA:
        arg = instr["arg"]

        if address_type == AddressType.STRAIGHT:
            arg = parse_io(command, arg)

            result = mnemonic_format.replace("%s", str(arg))
        elif address_type == AddressType.INDIRECT_DATA:
            result = mnemonic_format.replace("%s", f"#{hex(arg)}")
        elif address_type == AddressType.RELATIVE_INDIRECT:
            result = mnemonic_format.replace("%s", f"${hex(arg)}")
    elif command.type == OperationType.ARG_JMP:
        result = mnemonic_format.replace("%s", f'#{hex(instr["arg"])}')

    return result


def parse_io(command, arg):
    if command == Operation.LD:
        if arg == INPUT:
            arg = "INPUT"
        else:
            arg = hex(arg)

    if command == Operation.SV:
        if arg == OUTPUT:
            arg = "OUTPUT"
        elif arg == OUTPUT_NUM:
            arg = "OUTPUT_NUM"
        else:
            arg = hex(arg)

    return arg


def process(code_file, output_file):
    data, instr, settings = parse_asm_instructions(code_file.read())
    code = code_to_binary(data, instr, settings)
    print(f"bytes = {len(code)}")
    print(f"instr = {len(instr)}")
    mnemonic = code_to_mnemonic(data, instr, settings)
    with open("debug.txt", "w", encoding="utf-8") as file:
        file.write(mnemonic)
    output_file.write(code)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Translates code into binary file.")

    parser.add_argument(
        "src", type=argparse.FileType(encoding="utf-8"), metavar="source_file", help="file with asm code"
    )

    parser.add_argument(
        "--output",
        "-o",
        dest="dst",
        type=argparse.FileType("wb"),
        default="output",
        metavar="output_file",
        help="file for storing a binary executable (default: output)",
    )

    namespace = parser.parse_args()
    process(namespace.src, namespace.dst)
